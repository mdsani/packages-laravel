<?php

use Pondit\Masterdata\Http\Controllers\BrandController;


Route::get('/masterdata', function () {
    return "Hello Wolrd";
});

Route::get('/dashboard', function () {
    return view('masterdata::backend.dashboard');
})->name('dashboard');

//brands crud
Route::resource('brands', BrandController::class);
Route::get('/brands-trash', [BrandController::class, 'trash'])->name('brands.trash');
Route::get('/brands-trash/{brand}', [BrandController::class , 'restore'])->name('brands.restore');
Route::get('/brands-trash/{brand}/delete', [BrandController::class , 'delete'])->name('brands.delete');
Route::get('/brands-excel', [BrandController::class, 'excel'])->name('brands.excel');
Route::get('/brands-pdf', [BrandController::class, 'pdf'])->name('brands.pdf');
