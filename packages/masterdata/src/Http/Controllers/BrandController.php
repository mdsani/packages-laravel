<?php

namespace Pondit\Masterdata\Http\Controllers;
use Pondit\Masterdata\Models\Brand;

use App\Exports\BrandsExport;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class BrandController extends Controller
{
    public function index()
    {
        $brandCollection = Brand::latest();
        if(request('search')){
            $brand = $brandCollection
                            ->where('name','like', '%'.request('search').'%')
                            ->orWhere('description', 'like', '%'.request('search').'%');
        }
        $brand = $brandCollection->paginate(5);
            return view('masterdata::backend.brands.index',[
            'brands' => $brand,
        ]);

    }

    public function create()
    {
        return view('masterdata::backend.brands.create');
    }

    public function store(Request $request)
    {
        try{
            request()->validate([
                'name' => 'required',
                'description' => 'required'
            ]);
            Brand::create([
                'name' => $request->name,
                'description' =>$request->description
            ]);
            return redirect()->route('brands.index')->withMessage('Task was successful Created!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Brand $brand)
    {
        dd($brand);
        return view('masterdata::backend.brands.show',[
            'brand' => $brand
        ]);
    }

    public function edit($brandId)
    {
        $brand = Brand::findOrFail($brandId);

        return view('masterdata::backend.brands.edit',[
            'brand' =>$brand
        ]);
    }

    public function update(Request $request, $brandId)
    {
        try{
            request()->validate([
                'name' => 'required',
                'description' => 'required'
            ]);
            $brand = Brand::findOrFail($brandId);
            $brand->update([
                'name' => $request->name,
                'description' => $request->description
            ]);
            return redirect()->route('brands.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy($brandId)
    {
        try{
            $brand = Brand::findOrFail($brandId);
            $brand->delete();
            return redirect()->route('brands.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        try{
            $brand = Brand::onlyTrashed()->get();
            return view('masterdata::backend.brands.trash',[
                'brands' => $brand
            ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function restore($id)
    {
        try{
            $brand = Brand::onlyTrashed()->findOrFail($id);
            $brand->restore();
            return redirect()->route('brands.trash')->withMessage('Task was successfully Restore!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function delete($id)
    {
        try{
            $brand = Brand::onlyTrashed()->findOrFail($id);
            $brand->forceDelete();
            return redirect()->route('brands.trash')->withMessage('Task was successfully Permanent Delete!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function excel() 
    {
        return Excel::download(new BrandsExport, 'brand.xlsx');
    }

    public function pdf()
    {
        $brand = Brand::all();
        $pdf = PDF::loadView('backend.brands.pdf', ['brands' => $brand]);
        return $pdf->download('brands.pdf');
    }
}
