<x-backend.layouts.master>
    <x-slot name="page_title">
        Categories
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Categories
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Categories</a></li>
            <li class="breadcrumb-item active">Show</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Categories</span>
            <span>
                <a class="btn btn-primary text-left" href="{{ Route('categories.index') }}" role="button">List</a>
            </span>
        </div>
    </div>
    <div class="card-body">
        <table class="table border">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Product Name</th>
                    <th>Product Title</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>image1</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($category->products as $product)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $product->heading }}</td>
                    <td>{{ $product->title }}</td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->description }}</td>
                    <td><img src="{{ asset('storage/images/'.$product->image1) }}" alt=""></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</x-backend.layouts.master>



