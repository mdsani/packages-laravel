<x-backend.layouts.master>
    <x-slot name="page_title">
        Tags
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Tags
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item ">Tags</li>
                <li class="breadcrumb-item active">Trash</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Tags</span>
            @canany(['admin','editor'])
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('tags.index') }}" role="button">List</a>
                </span>
            @endcanany
        </div>
    </div>
    <div class="card-body">
        <x-backend.layouts.elements.message :message="session('message')"/>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Title</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach ($tags as $tag)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $tag->title }}</td>
                        <td>
                            <a onclick="return confirm('Are you sure want to restore ?')" class="btn btn-sm btn-primary mb-1" href="{{ route('tags.restore', ['tag' => $tag->id]) }}" role="button">Restore</a>
                            <a onclick="return confirm('Are you sure want to permanentdelete ?')" class="btn btn-sm btn-danger" href="{{ route('tags.delete', ['tag' => $tag->id]) }}" role="button">Permanent Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</x-backend.layouts.master>



