<x-backend.layouts.master>
    <x-slot name="page_title">
        Tags
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Tags
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Tags</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Tags</span>
            <span>
                <a class="btn btn-primary text-left" href="{{ Route('tags.index') }}" role="button">List</a>
            </span>
        </div>
    </div>
    <div class="card-body">
        <p>ID :{{ $tag->id }}</p>
        <p>Title :{{ $tag->title }}</p>
    </div>
</div>

</x-backend.layouts.master>



