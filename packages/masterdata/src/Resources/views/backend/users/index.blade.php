<x-backend.layouts.master>
    <x-slot name="page_title">
        Users
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Users
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Users</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Users</span>
                <span>
                    <a class="btn btn-sm btn-danger text-left" href="{{ route('users.trash') }}" role="button">Trash</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('users.excel') }}" role="button">Excel</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('users.pdf') }}" role="button">Pdf</a>
                </span>
        </div>   
    </div>
    <div class="card-body">
        <div class="d-flex justify-content-end mb-1">
            <form action="{{ route('users.index') }}" method="GET">
                <select name="role" class="form-select " style="width:220px">
                    <option value="" selected>Select Option</option>
                    @foreach ($roles as $role)
                        <option value="{{ $role->id }}"{{ $role->id==$req?'selected':'' }}>{{ $role->name }}</option>
                    @endforeach
                </select>
                <x-backend.form.button >
                    Submit
                  </x-backend.form.button>
            </form>
            <form action="{{ route('users.index') }}" method="GET">
                <x-backend.form.search name="search" placeholder="Search" style="width: 220px;"/>
            </form>
        </div>
        
        <x-backend.layouts.elements.message :message="session('message')"/>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Role Id</th>
                    <th>Role Name</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach ($users as $user)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $user->role_id}}</td>
                        <td>{{ $user->role->name}}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('users.show', ['user'=>$user->id]) }}" role="button">Show</a>
                            <a class="btn btn-sm btn-warning" href="{{ route('users.edit',['user' => $user->id]) }}" role="button">Edit</a>
                            <form style="display: inline;" action="{{ route('users.destroy', ['user'=>$user->id]) }}" method="POST">
                                @csrf
                                @method('delete')
                                <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</x-backend.layouts.master>



