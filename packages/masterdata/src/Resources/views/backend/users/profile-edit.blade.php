<x-backend.layouts.master>
    <x-slot name="page_title">
        Profile
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Profile
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Profile</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Profile</span>
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('users.index') }}" role="button">List</a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <x-backend.layouts.elements.errors :errors="$errors"/>
            <form action="{{ route('user.profile.update') }}" method="POST">
                @csrf
                @method('patch')
                <x-backend.form.input name="name" :value="$user->name"/>
                <x-backend.form.input name="email" type="email" :value="$user->email"/>
                <x-backend.form.input name="mobile_number" type="number" :value="old('mobile_number',$user->profile->mobile_number)"/>
                <x-backend.form.textarea name="address">
                    {{ old('address',$user->profile->address) }}
                </x-backend.form.textarea>
                <x-backend.form.textarea name="bio">
                    {{ old('bio',$user->profile->bio) }}
                </x-backend.form.textarea>
                <x-backend.form.input name="facebook_url" type="url" :value="old('facebook_url',$user->profile->facebook_url)"/>
                <x-backend.form.input name="twitter_url" type="url" :value="old('twitter_url',$user->profile->twitter_url)"/>

                

                <div class="mt-4 mb-0">
                    <div class="d-grid"><button class="btn btn-primary btn-block" type="submit">Save</button></div>
                </div>
            </form>
        </div>
    </div>

</x-backend.layouts.master>