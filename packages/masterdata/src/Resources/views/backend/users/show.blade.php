<x-backend.layouts.master>
    <x-slot name="page_title">
        Products
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Products
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Products</a></li>
            <li class="breadcrumb-item active">Show</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Products</span>
            <span>
                <a class="btn btn-primary text-left" href="{{ Route('users.index') }}" role="button">List</a>
            </span>
        </div>
    </div>
    <div class="card-body">
            <p>ID :{{ $user->id }}</p>
            <p>Role Name:{{ $user->role->name}}</p>
            <p>Name :{{ $user->name }}</p>
            <p>Email :{{ $user->email }}</p>
            <p>Mobile :{{ $user->profile->mobile_number }}</p>
            <p>Address :{{ $user->profile->address }}</p>
            <p>Bio :{{ $user->profile->bio }}</p>
            <p>Facebook Url :{{ $user->profile->facebook_url }}</p>
            <p>Twitter Url :{{ $user->profile->twitter_url }}</p>
    </div>
</div>

</x-backend.layouts.master>



