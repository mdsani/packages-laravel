<x-masterdata::backend.layouts.master>
    <x-slot name="page_title">
        Brands
    </x-slot>

    <x-slot name="breadcrumb">
        <x-masterdata::backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Brands
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('brands.index') }}">Brands</a></li>
            <li class="breadcrumb-item active">Create</li>
        </x-masterdata::backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Brands</span>
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('brands.index') }}" role="button">List</a>
                </span>
            </div>
        </div>
        <div class="card-body">
            @if($errors->any())
                <x-masterdata::backend.layouts.elements.errors :errors="$errors"/>
            @endif
            <form action="{{ route('brands.store') }}" method="POST">
                @csrf
                <div class="row mb-3">
                    <div class="col-md-12 mb-2">
                        <x-masterdata::backend.form.input name="name" :value="old('name')"/>
                    </div>

                    <div class="col-md-12">
                        <x-masterdata::backend.form.input name="description" :value="old('description')"/>
                    </div>
                </div>

                <div class="mt-4 mb-0">
                    <div class="d-grid"><button class="btn btn-primary btn-block" type="submit">Save</button></div>
                </div>
            </form>
        </div>
    </div>

</x-masterdata::backend.layouts.master>