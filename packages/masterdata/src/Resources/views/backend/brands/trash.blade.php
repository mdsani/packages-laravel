<x-masterdata::backend.layouts.master>
    <x-slot name="page_title">
        Brands
    </x-slot>

    <x-slot name="breadcrumb">
        <x-masterdata::backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Brands
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('brands.index') }}">Brands</a></li>
                <li class="breadcrumb-item active">Trash</li>
        </x-masterdata::backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Brands</span>
            <span>
                <a class="btn btn-primary text-left" href="{{ route('brands.index') }}" role="button">List</a>
            </span>
        </div>
    </div>
    <div class="card-body">
        <x-masterdata::backend.layouts.elements.message :message="session('message')"/>
        <table id="datatablesSimple">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                    @php
                        $sl = 0;
                    @endphp
                    @foreach ($brands as $brand)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $brand->name }}</td>
                        <td>{{ $brand->description }}</td>
                        <td>
                            <a onclick="return confirm('Are you sure want to restore ?')" class="btn btn-sm btn-primary mb-1" href="{{ route('brands.restore', ['brand' => $brand->id]) }}" role="button">Restore</a>
                            <a onclick="return confirm('Are you sure want to permanentdelete ?')" class="btn btn-sm btn-danger" href="{{ route('brands.delete', ['brand' => $brand->id]) }}" role="button">Permanent Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</x-masterdata::backend.layouts.master>



