<x-backend.layouts.master>
    <x-slot name="page_title">
        Roles
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Roles
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Roles</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Roles</span>
        </div>
    </div>
    <div class="card-body">
        
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach ($roles as $role)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $role->name }}</td>
                        <td><a class="btn btn-info" href="{{ route('roles.show',['role' => $role->id]) }}" role="button">Show</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</x-backend.layouts.master>



