<x-backend.layouts.master>
    <x-slot name="page_title">
        Sizes
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Sizes
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Sizes</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Sizes</span>
            @canany(['admin','editor'])
                <span>
                    <a class="btn btn-sm btn-primary text-left" href="{{ Route('sizes.create') }}" role="button">Add</a>
                    <a class="btn btn-sm btn-danger text-left" href="{{ route('sizes.trash') }}" role="button">Trash</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('sizes.excel') }}" role="button">Excel</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('sizes.pdf') }}" role="button">Pdf</a>
                </span>
            @endcanany
        </div>
    </div>
    <div class="card-body">
        <form action="{{ route('sizes.index') }}" method="GET">
            <x-backend.form.search name="search" placeholder="Search" style="width: 220px;"/>
        </form>
        <x-backend.layouts.elements.message :message="session('message')"/>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach ($sizes as $size)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $size->title }}</td>
                        <td>{{ $size->description }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('sizes.show', ['size'=>$size->id]) }}" role="button">Show</a>
                            @canany(['admin','editor'])
                                <a class="btn btn-sm btn-warning" href="{{ route('sizes.edit', ['size'=>$size->id]) }}" role="button">Edit</a>
                                <form style="display: inline;" action="{{ route('sizes.destroy', ['size'=>$size->id]) }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                                </form>
                            @endcanany
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</x-backend.layouts.master>



