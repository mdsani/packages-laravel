@props(['name'])
<div class="d-flex justify-content-end">
<input name="{{ $name }}" class="form-control" id="{{ $name }}" {{ $attributes }}>
</div>